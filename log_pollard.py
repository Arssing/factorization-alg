import math

def is_even(n: int):
    return int(n) & 1 == 0


def is_prime(num: int):
    if num == 2:
        return True
    limit = math.sqrt(num)
    i = 2
    while i <= limit:
        if num % i == 0:
            return False
        i = i + 1
    return True

def all_in_one(n, a):
    if n <= 1 or a <= 1:
        print("Числа не должны быть <= 1")
        return False
    elif not is_prime(n):
        print("Число n не яв-ся простым")
        return False
    elif n <= a:
        print("n <= a")
        return False
    else:
        return True

def get_S(x):
    if x % 3 == 1:
        return 1
    elif x % 3 == 2:
        return 2
    elif x % 3 == 0:
        return 3
def F(x, y, z, a, n, g):
    flag = get_S(x)
    f = n-1
    if flag == 1:
        return (a*x)%n, (y+1)%f, z
    elif flag == 2:
        return (x*x)%n, (2*y)%f, (2*z)%f
    elif flag == 3:
        return (g*x)%n, y, (z+1)%f

def GCD_bin(a, b):
#1
    if a == 0:
        return b
    elif b == 0:
        return a
    elif a == b:
        return a
#2        
    elif a == 1 or b == 1:
        return 1
#3 
    elif is_even(a) and is_even(b):
        return 2*GCD_bin(a >> 1, b >> 1)
#4
    elif is_even(a) and is_even(b) == False:
        return GCD_bin(a >> 1, b)
#5    
    elif is_even(a) == False and is_even(b):
        return GCD_bin(a, b >> 1)
#6
    elif a < b:
        return GCD_bin((b - a) >> 1, a)
#7
    elif a > b:
        return GCD_bin((a - b) >> 1, b)
        
#https://www.geeksforgeeks.org/eulers-totient-function/
def Euler_fun(n):
    result = n
    p = 2
    
    while p * p <= n:
        if n % p == 0:
            while n % p == 0:
                n = n / p
            result = result * (1 - (1 / p))
        p += 1
    
    if n > 1:
        result = result * (1 - (1 / n))
    return int(result)

#https://e-maxx.ru/algo/primitive_root
def generator(n):
    fi = Euler_fun(n)
    factor = []
    i = 2

    fi_cop = fi
    while i*i <= fi_cop:
        if fi_cop % i == 0:
            factor.append(i)
            while fi_cop % i == 0:
                fi_cop //= i
        i += 1
    if fi_cop > 1:
        factor.append(fi_cop)
    
    
    for g in range(2, n):
        if GCD_bin(g, n) == 1:
            count = 0
            for pi in factor:
                if g ** (fi // pi) % n != 1:
                    count += 1
                if count == len(factor):
                    return g

    return -1
    
def log_Poll(g, n, a):
#1
    x1 = x2 = 1
    y1 = y2 = z1 = z2 = 0
#2
    x1, y1, z1 = F(x1,y1,z1,a,n,g)
    x2,y2,z2 = F(*F(x2,y2,z2,a,n,g),a,n,g)
#3
    while x1 != x2:
        x1, y1, z1 = F(x1,y1,z1,a,n,g)
        x2, y2, z2 = F(*F(x2,y2,z2,a,n,g),a,n,g)
#4
    if y1 == y2:
        return -1
#5
    r = (y1 - y2) % (n-1)
    d = math.gcd(r, n-1)
    right_side = (z2-z1) % (n-1)
    resheniya = []
    for x in range(2,n):
        if (x * r) % (n-1) == right_side:
            resheniya.append(x)
        if len(resheniya) == d:
            break

    for r_ in resheniya:
        if (g ** r_) % n == a:
            return r_

if __name__ == '__main__':
    print("Дискретное логарифмирование ро-метод Полларда: ")
    n = int(input("Введите n:"))
    a = int(input("Введите a:"))
    if all_in_one(n,a):
        g = generator(n)
        otvet = log_Poll(g,n,a)
        print("log"+str(g)+"("+str(a)+")"+"= "+str(otvet)+" mod "+str(n))
    else:
        print("Проверка завершилась с ошибкой")
    