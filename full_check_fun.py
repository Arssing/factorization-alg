import math

def is_even(n: int):
    return int(n) & 1 == 0

def next_divider(prev: int):
    prev = prev + 1
    while not is_prime(prev):
        prev = prev + 1
    return prev

def check_nk(num: int):
    k = 2
    kmax = int(math.log2(num))
    while k <= kmax:
        x = num ** (1/k)
        if x.is_integer():
            return True
        k = next_divider(k)
    return False

def try_conver_to_positive_int(n):
    try:
        n = int(n)
    except:
        return -1
    if n < 0:
        return -2
    return n


def is_prime(num: int):
    if num == 2:
        return True
    limit = math.sqrt(num)
    i = 2
    while i <= limit:
        if num % i == 0:
            return False
        i = i + 1
    return True


def all_in_one_Dixon(x):
    n = try_conver_to_positive_int(x)
    if n == -1:
        print("Ошибка: не удалось преобразовать число из str в int")
    elif n == -2:
        print("Ошибка: число < 0")
    elif is_even(n):
        print("Ошибка: чётное число")
    elif is_prime(n):
        print("Ошибка: простое число")
    elif check_nk(n):
        print("Ошибка: число представимо в виде x^k")
    else:
        return True
    return False
