import math

def is_even(n: int):
    return int(n) & 1 == 0

def next_divider(prev: int):
    prev = prev + 1
    while not is_prime(prev):
        prev = prev + 1
    return prev

def check_nk(num: int):
    k = 2
    kmax = int(math.log2(num))
    while k <= kmax:
        x = num ** (1/k)
        if x.is_integer():
            return True
        k = next_divider(k)
    return False

def try_conver_to_positive_int(n):
    try:
        n = int(n)
    except:
        return -1
    if n < 0:
        return -2
    return n


def is_prime(num: int):
    if num == 2:
        return True
    limit = math.sqrt(num)
    i = 2
    while i <= limit:
        if num % i == 0:
            return False
        i = i + 1
    return True

def all_in_one(x):
    n = try_conver_to_positive_int(x)
    if n == -1:
        print("Ошибка: не удалось преобразовать число из str в int")
    elif n == -2:
        print("Ошибка: число < 0")
    elif is_even(n):
        print("Ошибка: чётное число")
    elif is_prime(n):
        print("Ошибка: простое число")
    elif check_nk(n):
        print("Ошибка: число представимо в виде x^k")
    else:
        return True
    return False

def add_lists(a: list, b: list):
    result = []
    if len(a)!= len(b):
        return -1
    else:
        for in_a, in_b in zip(a,b):
            result.append(in_a+in_b)
        return result
def Gauss(A): # A[mt * nt]
    mt = len(A)
    nt = len(A[0])
    #создание единичной матрицы
    E = []
    for i in range(0, mt):
        add = []
        for j in range(0, mt):
            if j == i:
                add.append(1)
            else:
                add.append(0)
        E.append(add)
    k = 0
    #проходим матрицу
    while k < nt:
        j = k
        #поиск ненулевого элемента в столбцах k...
        while j < mt and A[j][k] == 0:
            j += 1
        #если столбец пройден, то переходим на новый
        if j >= mt:
            k += 1
            continue
        #если строка j с ненулевым элементом ниже строки k, то меняем их местами
        if j > k:
            tmp = A[k]
            A[k] = A[j]
            A[j] = tmp

            tmp = E[k]
            E[k] = E[j]
            E[j] = tmp
        i = k+1

        while i < mt:
            #если строки ниже также содержат единиицу, то мы их "обнуляем"
            if A[i][k] == 1:
                for j in range(0, nt):
                    A[i][j] = (A[i][j] + A[k][j]) % 2
                for j in range(0, mt):
                    E[i][j] = (E[i][j] + E[k][j]) % 2
            i += 1
        k += 1
    
    current_list = []
    
    for i in range(mt-1,-1,-1):
        count = 0
        for j in range(0, nt):
            
            if A[i][j] == 1:
                break
            count += 1

        if count == nt:
            current_list.append(i)
    
    otvet = []
    for i in current_list:
        otvet.append(E[i])
    
    return otvet

def get_stepeni_by_razl(razl):
    o = 2
    stepeni = []
    if razl[0] == -1:
        razl.remove(-1)
        stepeni.append(1)
    else:
        stepeni.append(0)
    stepeni.append(0)
    count_step = 1

    while razl:
        try:
            razl.remove(o)
        except:
            stepeni.append(0)
            count_step += 1
            o = next_divider(o)
            continue
        stepeni[count_step] += 1

    return stepeni

def jacobi(a, p):
    a = a % p
    t = 1
    while a != 0:
        while not int(a) & 1:
            a /= 2
            r = p % 8
            if r == 3 or r == 5:
                t = -t
        a, p = p, a
        if a % 4 == p % 4 == 3:
            t = -t
        a %= p
    if p == 1:
        return t
    else:
        return 0

def Square(n, k):#кол-во чисел в ФБ (>=3)
    if k < 3:
        return None
#1
    S = [-1, 2]
    value_to_gen_div = 2
    
    while len(S) != k:
        test_p = next_divider(value_to_gen_div)
        if jacobi(n, test_p) == 1:
            S.append(test_p)
        value_to_gen_div = test_p
        continue
    m = int(math.sqrt(n))

    x = 0
    i = 0
    e = []
    a_list = []
    count = 0
    t = k + 1
#2
    while True:
        while i != t:
            a = x + m
            b = a ** 2 - n

            if count & 1:
                x = -x
            else:
                x = -x + 1
            count += 1

            e_test = []
            for s in S:
                if s == -1:
                    if b < 0:
                        e_test.append(1)
                        b*= -1
                    else:
                        e_test.append(0)
                    continue
                j = 0
                while b % s == 0:
                    b //= s
                    j += 1
                e_test.append(j)

            check = 0
            for num in e_test:
                if num == 0:
                    check += 1
            if check == len(e_test) and b!=1:
                continue
            if b == 1:
                e.append(e_test)
                a_list.append(a)
                i += 1
#3  
        #copy matrix
        V = []
        for row in e:
            test_ = []
            for char in row:
                test_.append(char)
            V.append(test_)
        for i in range(len(V)):
            for j in range(len(V[0])):
                V[i][j] = V[i][j] % 2 
        solutions = Gauss(V)
#4  
        for sol in solutions:
            x_ = 1
            pow_list = [0 for char in e[0]]
#5  
            for i in range(len(sol)):
                if sol[i] == 1:
                    x_ = (x_*a_list[i]) % n
                    pow_list = add_lists(pow_list, e[i])

            for i in range(len(pow_list)):
                pow_list[i] //= 2
            y = 1
            for i in range(len(S)):
                y = (y*S[i]**pow_list[i]) % n
#7  
            if x !=abs(y):
                result1 = math.gcd(x_-y,n)
                result2 = math.gcd(x_+y,n)
                if result1 != 1 and result2 != 1:
                    return result1, result2
                else:
                    continue

        t += 1


if __name__ == '__main__':
    n = int(input("Введите число: "))
    k = int(input("Введите k - кол-во простых чисел для факторной базы S (>=3): "))
    if all_in_one(n) and k >= 3:
        print(Square(n,k))
    else:
        print("Проверка завершилась с ошибкой")
    