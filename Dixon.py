import math
import numpy as np
import sys
from random import randint
from full_check_fun import next_divider, is_prime, check_nk, all_in_one_Dixon

def Gauss(A): # A[mt * nt]
    mt = len(A)
    nt = len(A[0])
    
    E = []
    for i in range(0, mt):
        add = []
        for j in range(0, mt):
            if j == i:
                add.append(1)
            else:
                add.append(0)
        E.append(add)
    k = 0

    while k < nt:
        j = k
        while j < mt and A[j][k] == 0:
            j += 1
           
        if j >= mt:
            k += 1
            continue
        if j > k:
            tmp = A[k]
            A[k] = A[j]
            A[j] = tmp

            tmp = E[k]
            E[k] = E[j]
            E[j] = tmp
        i = k+1
        while i < mt:
            if A[i][k] == 1:
                for j in range(0, nt):
                    A[i][j] = (A[i][j] + A[k][j]) % 2
                for j in range(0, mt):
                    E[i][j] = (E[i][j] + E[k][j]) % 2
            i += 1
        k += 1
    
    current_list = []
    
    for i in range(mt-1,-1,-1):
        count = 0
        for j in range(0, nt):
            
            if A[i][j] == 1:
                break
            count += 1

        if count == nt:
            current_list.append(i)
    
    otvet = []
    for i in current_list:
        otvet.append(E[i])
    
    return otvet

        
def add_valid_a_and_b(a: list, b: list, n: int, limit_for_try_method: int):
    while True:
        new_num = randint(2, n)
        try:
            a.index(new_num)
        except:
            bi = new_num ** 2 % n
            if is_prime(bi) or check_nk(bi):
                continue
            list_div = try_method(bi, limit_for_try_method)
            if not list_div:
                continue

            a.append(new_num)
            b.append(bi)
            return a, b, list_div

def get_stepeni_by_razl(razl):
    o = 2
    stepeni = [0]
    count_step = 0

    while razl:
        try:
            razl.remove(o)
        except:
            stepeni.append(0)
            count_step += 1
            o = next_divider(o)
            continue
        stepeni[count_step] += 1

    return stepeni

def get_next_x_by_solution(a: list, list_of_prime: list):
    ln = len(a)
    ai = []
    for i in range(0, ln):
        if a [i] == 1:
            ai.append(list_of_prime[i])
    multi = 1
    for num in ai:
        multi *= num
    return multi


def get_next_y_by_solution(solution, e, list_of_prime):
    sum_step = [0 for c in e[0]]
    l = len(e[0])
    for i in range(len(solution)):
            if solution[i] == 1:
                for j in range(l):
                    sum_step[j] += e[i][j]
    for i in range(len(sum_step)):
        sum_step[i] //= 2
    y = 1
    for i,j in zip(list_of_prime, sum_step):
        y = y*(i ** j)
    return y


def Dixon_alg(n: int, k: int):#k - кол-во простых чисел для факторной базы S 
#1
    list_of_prime = [2]
    for i in range (0, k-1):
        list_of_prime.append(next_divider(list_of_prime[i]))
#2
    t = k+1
    a = []
    b = []
    e = []

    for i in range(0, t):
#2.1
        a, b, razl = add_valid_a_and_b(a, b, n, list_of_prime[-1])
#2.2
        stepeni = get_stepeni_by_razl(razl)
        
        while len(stepeni) < k:
            stepeni.append(0)
        e.append(stepeni)
#3
    while True:
        V = []
        for row in e:
            e2 = []
            for elem in row:
                e2.append(elem % 2)
            V.append(e2)
#4      
        list_c = Gauss(V)
#5
        for solution in list_c:
#6  
            x = get_next_x_by_solution(solution, a) % n
            y = get_next_y_by_solution(solution, e, list_of_prime)

            if x != abs(y):
                otvet = []
                otvet.append(math.gcd(x-y,n))
                otvet.append(math.gcd(x+y,n))
                if otvet[0] == 1 or otvet[1] == 1:
                    continue
                return otvet
            else:
                continue
        t += 1
        a, b, razl = add_valid_a_and_b(a, b, n, list_of_prime[-1])
        stepeni = get_stepeni_by_razl(razl)
        while len(stepeni) < k:
            stepeni.append(0)
        e.append(stepeni)
        continue


def try_method(num, B):#B - верхняя граница
#1
    d = [2]
    p = []
    k = 0
    while True:
#2
        if num == 1:
            break
#3
        r = num % d[k]
        q = num // d[k]
#4      
        if r == 0:
            p.append(d[k])
            num = q
            continue
#5
        if d[k] < q:
            d.append(next_divider(d[k]))
            k = k + 1

            if d[k] > B:
                return None
            continue
#6
        if num > B:
            return None
        p.append(num)
        break
    return p

if __name__ == '__main__':
    n = input("Введите число: ")
    k = int(input("Введите k - кол-во простых чисел для факторной базы S: "))
    if all_in_one_Dixon(n) and k > 0:
        n = int(n)

        otvet = Dixon_alg(n, k)
        print(otvet)

    else:
        print("Проверка завершилась с ошибкой")